# import PIL.Image

# im = PIL.Image.open("sp_noise.png")
# im = im.convert('L')

# for i in range(2,im.size[0]-2):
#     for j in range(2,im.size[1]-2):
#         b = []
#         if im.getpixel((i,j))>0 and im.getpixel((i,j))<255:
#             pass
#         elif im.getpixel((i,j))==0 or im.getpixel((i,j))==255:
#             c=0
#             for p in range(i-1,i+2):
#                 for q in range(j-1,j+2):
#                     if im.getpixel((p,q))==0 or im.getpixel((p,q))==255: 
#                         c=c+1
#             if c>6:
#                 c=0
#                 for p in range(i-2,i+3):
#                     for q in range(j-2,j+3):
#                         b.append(im.getpixel((p,q)))
#                         if im.getpixel((p,q))==0 or im.getpixel((p,q))==255:
#                             c=c+1
#                 if c==25:
#                     a=sum(b)/25
#                     # print a
#                     im.putpixel((i,j),a)
#                 else:
#                     p=[]
#                     for t in b:
#                         if t not in (0,255):
#                             p.append(t)
#                     p.sort()
#                     im.putpixel((i,j),p[len(p)/2])
#             else:
#                 b1=[]
#                 for p in range(i-1,i+2):
#                     for q in range(j-1,j+2):
#                         b1.append(im.getpixel((p,q)))
#                 im.putpixel((i,j),sum(b1)/9)

# im.save("nonoise.jpg")  

import numpy as np
import cv2
import os

# f = open('mnisttest/000000-num7.jpg', 'r')

img = cv2.imread('000000-num7.jpg', cv2.IMREAD_GRAYSCALE)
# row,ch = img.shape

def Salt_Pepper(img ,factor = 0.5, threshold = 0.009):
    noisy = img
    # Salt mode
    num_salt = np.ceil(factor * img.size * threshold)
    coords = [np.random.randint(0, i - 1, int(num_salt))
              for i in img.shape]
    noisy[coords] = 255

    # Pepper mode
    num_pepper = np.ceil(factor * img.size * (1. - threshold))
    coords = [np.random.randint(0, i - 1, int(num_pepper))
              for i in img.shape]
    noisy[coords] = 0

    return noisy


# def show_images(images):
#     plt.figure(figsize=(10, 2))
#     for i in range(1):
#         plt.subplot(2, 10, i+1)
#         plt.imshow(images[i].reshape(28, 28), cmap='gray')
#         plt.xticks([])
#         plt.yticks([])
#     plt.show()

cv2.imshow('noisy', Salt_Pepper(img))
cv2.waitKey(0) 
cv2.destroyAllWindows()
cv2.imwrite('noise.png', Salt_Pepper(img))
