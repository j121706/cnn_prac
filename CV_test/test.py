import cv2 
import numpy as np 
import os
from matplotlib import pyplot as plt 

def noisy(noise_typ,image):
    if noise_typ == "gauss":
        row,col,ch= image.shape
        mean = 0
        var = 0.1
        sigma = var**0.5
        gauss = np.random.normal(mean,sigma,(row,col,ch))
        gauss = gauss.reshape(row,col,ch)
        noisy = image + gauss
        return noisy
    elif noise_typ == "s&p":
        row,col,ch = image.shape
        s_vs_p = 0.5
        amount = 0.004
        out = np.copy(image)
        # Salt mode
        num_salt = np.ceil(amount * image.size * s_vs_p)
        coords = [np.random.randint(0, i - 1, int(num_salt))
                for i in image.shape]
        out[coords] = 1 
        # Pepper mode
        num_pepper = np.ceil(amount* image.size * (1. - s_vs_p))
        coords = [np.random.randint(0, i - 1, int(num_pepper))
                for i in image.shape]
        out[coords] = 0
        return out
    elif noise_typ == "poisson":
        vals = len(np.unique(image))
        vals = 2 ** np.ceil(np.log2(vals))
        noisy = np.random.poisson(image * vals) / float(vals)
        return noisy
    elif noise_typ =="speckle":
        row,col,ch = image.shape
        gauss = np.random.randn(row,col,ch)
        gauss = gauss.reshape(row,col,ch)        
        noisy = image + image * gauss
        return noisy# import numpy as np


img = cv2.imread('lena_Ori.bmp',cv2.IMREAD_UNCHANGED) 
# px = img[100,100]
# img[100,100] = [0,0,0]
# px = img[100,100]

out = noisy("s&p", img)
img_out = cv2.cvtColor(out, cv2.COLOR_BGR2GRAY)

cv2.imshow('image',img_out) 
cv2.waitKey(0) 
cv2.destroyAllWindows()
cv2.imwrite('lena_noise.png',img_out)


# import cv2 
# import numpy as np 
# from matplotlib import pyplot as plt 

# img = cv2.imread('lena_Ori.bmp',cv2.IMREAD_GRAYSCALE) 
# plt.imshow(img, cmap = 'gray', interpolation = 'bicubic') 
# plt.xticks([]), plt.yticks([]) # to hide tick values on X and Y axis 
# plt.plot([200,300,400],[100,200,300],'c', linewidth=5) 
# plt.show()
# cv2.imwrite('lena_gray.png',img)



