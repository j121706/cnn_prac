from skimage import io
import numpy
import numpy as np
import random
import math
import cv2
import keras
import tensorflow as tf
from matplotlib import pyplot as plt

def sp_noise(image,prob = 0.02):
    '''
    Add salt and pepper noise to image
    prob: Probability of the noise
    '''
    output = np.zeros(image.shape, np.uint8)
    thres = 1 - prob 
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            rdn = random.random()   # 0 ~ 1
            if rdn < prob:  #rdn < 0.02
                output[i][j] = 0
            elif rdn > thres:   #rdn > 0.98
                output[i][j] = 255
            else:
                output[i][j] = image[i][j]
    # print("rdn =", rdn)
    return output


def sp_noise_2(image):
    row, col = image.shape
    s_vs_p = 0.5
    amount = 0.004
    out = np.copy(image)
    # Salt mode
    num_salt = np.ceil(amount * image.size * s_vs_p)
    coords = [np.random.randint(0, i - 1, int(num_salt))
            for i in image.shape]
    out[coords] = 255
    # Pepper mode
    num_pepper = np.ceil(amount * image.size * (1. - s_vs_p))
    coords = [np.random.randint(0, i - 1, int(num_pepper))
              for i in image.shape]
    out[coords] = 0
    return out


def gauss(image):
    row,col= image.shape
    mean = 0
    var = 0.5
    sigma = var**0.5
    gauss = np.random.normal(mean,sigma,(row,col))
    gauss = gauss.reshape(row,col)
    noisy = image + gauss
    return noisy

def poisson(image):
    vals = len(np.unique(image))
    vals = 2 ** np.ceil(np.log2(vals))
    noisy = np.random.poisson(image * vals) / float(vals)
    return noisy


def speckle(image):
    row, col = image.shape
    gauss = np.random.randn(row, col)
    gauss = gauss.reshape(row, col)
    noisy = image + image * gauss
    return noisy


image = cv2.imread('000000-num7.jpg', 0)  # Only for grayscale image
# cv2.imshow('Ori', image)
# cv2.waitKey(0)
# cv2.destroyAllWindows()
# noise_img = sp_noise(image,0.02)
noise_img = sp_noise(image)
cv2.imwrite('SPnoise.png', noise_img)

img = cv2.imread('noise.png')
dst = cv2.fastNlMeansDenoising(img, h=60,templateWindowSize=7, searchWindowSize=21)
cv2.imwrite('denoise.png', dst)
plt.subplot(121), plt.imshow(img)
plt.subplot(122), plt.imshow(dst)
plt.show()

# Compute PSNR over tf.float32 Tensors.
# raw1 = tf.io.gfile.GFile('000001-num2.png','r').read()
# raw2 = tf.io.gfile.GFile('denoise.png','r').read()

im1 = tf.image.decode_image(tf.io.read_file('000001-num2.png'))
im2 = tf.image.decode_image(tf.io.read_file('denoise.png'))
im3 = tf.image.decode_image(tf.io.read_file('speckle_noise.png'))


# im1 = tf.image.convert_image_dtype(im1, tf.float32)
# im2 = tf.image.convert_image_dtype(im2, tf.float32)
# im3 = tf.image.convert_image_dtype(im3, tf.float32)

# print(im1.numpy().shape, im2.numpy().shape, im3.numpy().shape)
im1 = im1.numpy()
im2 = im2.numpy()
im3 = im3.numpy()

# im2 = (im2[:,:,0]+im2[:,:,1]+im2[:,:,2])/3
# im3 = (im3[:, :, 0]+im3[:, :, 1]+im3[:, :, 2])/3

# import numpy, math

# def psnr(img1, img2):
#     mse = numpy.mean((img1/1.0 - img2/1.0) ** 2)
#     if mse == 0:
#         return 100
#     PIXEL_MAX = 1.0
#     return 20 * math.log10(PIXEL_MAX / math.sqrt(mse))


# print("PSNR", psnr(im1, im3), " dB")


def log10(x):
    numerator = tf.math.log(x)
    denominator = tf.math.log(tf.constant(10, dtype=numerator.dtype))
    return numerator / denominator


def psnr(im1, im2):
    img_arr1 = numpy.array(im1).astype('float32')
    img_arr2 = numpy.array(im2).astype('float32')
    mse = tf.reduce_mean(tf.math.squared_difference(img_arr1, img_arr2))
    psnr = tf.constant(255**2, dtype=tf.float32)/mse
    result = tf.constant(10, dtype=tf.float32)*log10(psnr)
    # with tf.Session():   
    # result = result.eval()
    return result


print("PSNR", psnr(im1, im3), " dB")
# psnr2 = tf.image.psnr(im1, im2, max_val=1.0)
# print('PSNR = %4.2f', psnr2)
# psnr1 and psnr2 both have type tf.float32 and are almost equal.

