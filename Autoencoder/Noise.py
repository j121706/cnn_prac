import numpy as np
import keras
import cv2
import random
from keras.layers import Input, Dense, Conv2D, MaxPooling2D, UpSampling2D
from keras.models import Model, load_model
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

(X_train, _), (X_test, _) = keras.datasets.mnist.load_data()


def preprocess(x):
    x = x.astype('float32') / 255.
    return x.reshape(-1, np.prod(x.shape[1:]))  # flatten

def make_convolutional_autoencoder():
    # encoding
    inputs = Input(shape=(28, 28, 1))
    x = Conv2D(16, 3, activation='relu', padding='same')(inputs)
    x = MaxPooling2D(padding='same')(x)
    x = Conv2D(8, 3, activation='relu', padding='same')(x)
    x = MaxPooling2D(padding='same')(x)
    x = Conv2D(8, 3, activation='relu', padding='same')(x)
    encoded = MaxPooling2D(padding='same')(x)

    # decoding
    x = Conv2D(8, 3, activation='relu', padding='same')(encoded)
    x = UpSampling2D()(x)
    x = Conv2D(8, 3, activation='relu', padding='same')(x)
    x = UpSampling2D()(x)
    x = Conv2D(16, 3, activation='relu')(x)  # <= padding='valid'!
    x = UpSampling2D()(x)
    decoded = Conv2D(1, 3, activation='sigmoid', padding='same')(x)

    # autoencoder
    autoencoder = Model(inputs, decoded)
    autoencoder.compile(optimizer='adam', loss='binary_crossentropy')
    return autoencoder

def show_images(before_images, after_images):
    plt.figure(figsize=(10, 2))
    for i in range(1):
        # before
        plt.subplot(2, 10, i+1)
        plt.imshow(before_images[i].reshape(28, 28), cmap='gray')
        plt.xticks([])
        plt.yticks([])
        # after
        plt.subplot(2, 10, 10+i+1)
        plt.imshow(after_images[i].reshape(28, 28), cmap='gray')
        plt.xticks([])
        plt.yticks([])
    plt.show()

def add_noise(x, noise_factor=0.2):
    x = x + np.random.randn(*x.shape) * noise_factor
    x = x.clip(0., 1.)
    return x


def add_s_p(x,  prob = 0.02):
    thres = 1 - prob
    for i in range(28*28):
        rdn = random.random()   # 0 ~ 1
        if rdn < prob:  # rdn < 0.02
            x = 0;
            # output[i][j] = 0
        elif rdn > thres:  # rdn > 0.98
            x = 255;
            # output[i][j] = 255
        else:
            x = x;
            # output[i][j] = image[i][j]
    # x = x + np.random.randn(*x.shape) * noise_factor
    # x = x.clip(0., 1.)
    return x
    

def sp_noise(image, prob = 0.02):
    '''
    Add salt and pepper noise to image
    prob: Probability of the noise
    '''
    output = np.zeros(image.shape, np.uint8)
    thres = 1 - prob
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            rdn = random.random()   # 0 ~ 1
            if rdn < prob:  # rdn < 0.02
                output[i][j] = 0
            elif rdn > thres:  # rdn > 0.98
                output[i][j] = 255
            else:
                output[i][j] = image[i][j]
    # print("rdn =", rdn)
    return output

X_train, X_valid = train_test_split(X_train, test_size=500)

# X_train = X_train.reshape(-1, 28, 28, 1)
# X_valid = X_valid.reshape(-1, 28, 28, 1)
# X_test = X_test.reshape(-1, 28, 28, 1)

X_train_noisy = add_s_p(X_train)
X_valid_noisy = add_s_p(X_valid)
X_test_noisy = add_s_p(X_test)


X_train_noisy = X_train.reshape(-1, 28, 28, 1)
X_valid_noisy = X_valid.reshape(-1, 28, 28, 1)
X_test_noisy = X_test.reshape(-1, 28, 28, 1)

# X_train_noisy = add_noise(X_train)
# X_valid_noisy = add_noise(X_valid)
# X_test_noisy  = add_noise(X_test)


# noise_factor = 0.5
# X_train_noisy = X_train + noise_factor * \
#     np.random.normal(loc=0.0, scale=1.0, size=x_train.shape)
# X_test_noisy = X_test + noise_factor * \
#     np.random.normal(loc=0.0, scale=1.0, size=x_test.shape)

# X_train_noisy = np.clip(X_train_noisy, 0., 1.)
# X_test_noisy = np.clip(X_test_noisy, 0., 1.)

# X_train[0].shape
# plt.figure(figsize=(10, 5))
# for i in range(10):
#     plt.subplot(1, 10, i+1)
#     plt.imshow(X_train[i], cmap='gray')
#     plt.xticks([])
#     plt.yticks([])
# plt.show()

# inputs  = Input(shape=(784,))              # 28*28 flatten
# enc_fc  = Dense( 32, activation='relu')    # compress using 32 neurons
# encoded = enc_fc(inputs)

# dec_fc  = Dense(784, activation='sigmoid') # decompress to 784 pixels
# decoded = dec_fc(encoded)

# build the model to train
# autoencoder = Model(inputs, decoded)
# autoencoder.compile(optimizer='adam', loss='binary_crossentropy')

# autoencoder = make_convolutional_autoencoder()
# autoencoder.fit(X_train_noisy, X_train, epochs=1, batch_size=128, validation_data=(X_valid_noisy, X_valid))

X_test_noisy = cv2.imread('speckle_noise.png', 0)
print(X_test_noisy.shape)
X_test_noisy = preprocess(X_test_noisy)
X_test_noisy = X_test_noisy.reshape(-1, 28, 28, 1)
print(X_test_noisy.shape)
autoencoder = load_model("Convolution_100.h5")

X_test_decoded = autoencoder.predict(X_test_noisy)
print(X_test_noisy.shape, X_test_decoded.shape)
show_images(X_test_noisy, X_test_decoded)

# X_test_decoded = X_test_decoded.reshape(28,28)
cv2.imwrite('decoded.png', X_test_decoded)
