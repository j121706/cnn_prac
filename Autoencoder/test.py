import numpy as np
import keras
from keras.layers import Input, Dense, Conv2D, MaxPooling2D, UpSampling2D
from keras.models import Model, load_model
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

(X_train, _), (X_test, _) = keras.datasets.mnist.load_data()

# X_train[0].shape
# plt.figure(figsize=(10, 5))
# for i in range(10):
#     plt.subplot(1, 10, i+1)
#     plt.imshow(X_train[i], cmap='gray')
#     plt.xticks([])
#     plt.yticks([])
# plt.show()

# inputs  = Input(shape=(784,))              # 28*28 flatten
# enc_fc  = Dense( 32, activation='relu')    # compress using 32 neurons
# encoded = enc_fc(inputs)

# dec_fc  = Dense(784, activation='sigmoid') # decompress to 784 pixels
# decoded = dec_fc(encoded)

# build the model to train
# autoencoder = Model(inputs, decoded)
# autoencoder.compile(optimizer='adam', loss='binary_crossentropy')


def preprocess(x):
    x = x.astype('float32') / 255.
    return x.reshape(-1, np.prod(x.shape[1:])) # flatten

X_train = preprocess(X_train)
X_test  = preprocess(X_test)

# also create a validation set for training
# X_train, X_valid = train_test_split(X_train, test_size=500, shuffer=False)
# autoencoder.fit(X_train, X_train, epochs=30, batch_size=128, validation_data=(X_valid, X_valid))

# load trained model
autoencoder = load_model("test_model.h5")

print(autoencoder.weights)

print("====================")

encoder = load_model("encoder.h5")
decoder = load_model("decoder.h5")
print(encoder.weights)
print("====================")
print(decoder.weights)

# decoder_inputs = Input(shape=(32,))
# decoder = Model(decoder_inputs, dec_fc(decoder_inputs))
# dec_fc  = Dense(784, activation='sigmoid') # decompress to 784 pixels
# decoded = dec_fc(encoded)
X_test_encoded = encoder.predict(X_test)

X_test_decoded = decoder.predict(X_test_encoded)

def show_images(before_images, after_images):
    plt.figure(figsize=(10, 2))
    for i in range(10):
        # before
        plt.subplot(2, 10, i+1)
        plt.imshow(before_images[i].reshape(28, 28), cmap='gray')
        plt.xticks([])
        plt.yticks([])
        # after
        plt.subplot(2, 10, 10+i+1)
        plt.imshow(after_images[i].reshape(28, 28), cmap='gray')
        plt.xticks([])
        plt.yticks([])
    plt.show()
    
show_images(X_test, X_test_decoded)
