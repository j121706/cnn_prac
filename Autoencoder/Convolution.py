import numpy as np
import keras
import cv2
from keras.layers import Input, Dense, Conv2D, MaxPooling2D, UpSampling2D
from keras.models import Model, load_model
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

(X_train, _), (X_test, _) = keras.datasets.mnist.load_data()


def preprocess(x):
    x = x.astype('float32') / 255.
    return x.reshape(-1, np.prod(x.shape[1:]))  # flatten

def make_convolutional_autoencoder():
    # encoding
    inputs = Input(shape=(28, 28, 1))
    x = Conv2D(16, 3, activation='relu', padding='same')(inputs)
    x = MaxPooling2D(padding='same')(x)
    x = Conv2D(8, 3, activation='relu', padding='same')(x)
    x = MaxPooling2D(padding='same')(x)
    x = Conv2D(8, 3, activation='relu', padding='same')(x)
    encoded = MaxPooling2D(padding='same')(x)

    # decoding
    x = Conv2D(8, 3, activation='relu', padding='same')(encoded)
    x = UpSampling2D()(x)
    x = Conv2D(8, 3, activation='relu', padding='same')(x)
    x = UpSampling2D()(x)
    x = Conv2D(16, 3, activation='relu')(x)  # <= padding='valid'!
    x = UpSampling2D()(x)
    decoded = Conv2D(1, 3, activation='sigmoid', padding='same')(x)

    # autoencoder
    autoencoder = Model(inputs, decoded)
    autoencoder.compile(optimizer='adam', loss='binary_crossentropy')
    return autoencoder

def show_images(before_images, after_images):
    plt.figure(figsize=(10, 2))
    for i in range(10):
        # before
        plt.subplot(2, 10, i+1)
        plt.imshow(before_images[i].reshape(28, 28), cmap='gray')
        plt.xticks([])
        plt.yticks([])
        # after
        plt.subplot(2, 10, 10+i+1)
        plt.imshow(after_images[i].reshape(28, 28), cmap='gray')
        plt.xticks([])
        plt.yticks([])
    plt.show()

# noise_factor = 0.5
# X_train_noisy = X_train + noise_factor * \
#     np.random.normal(loc=0.0, scale=1.0, size=x_train.shape)
# X_test_noisy = X_test + noise_factor * \
#     np.random.normal(loc=0.0, scale=1.0, size=x_test.shape)

# X_train_noisy = np.clip(X_train_noisy, 0., 1.)
# X_test_noisy = np.clip(X_test_noisy, 0., 1.)

# X_train[0].shape
# plt.figure(figsize=(10, 5))
# for i in range(10):
#     plt.subplot(1, 10, i+1)
#     plt.imshow(X_train[i], cmap='gray')
#     plt.xticks([])
#     plt.yticks([])
# plt.show()

# inputs  = Input(shape=(784,))              # 28*28 flatten
# enc_fc  = Dense( 32, activation='relu')    # compress using 32 neurons
# encoded = enc_fc(inputs)

# dec_fc  = Dense(784, activation='sigmoid') # decompress to 784 pixels
# decoded = dec_fc(encoded)

# build the model to train
# autoencoder = Model(inputs, decoded)
# autoencoder.compile(optimizer='adam', loss='binary_crossentropy')

autoencoder = make_convolutional_autoencoder()
autoencoder.summary()


X_train = preprocess(X_train)
X_test  = preprocess(X_test)

# also create a validation set for training
X_train, X_valid = train_test_split(X_train, test_size=500)
# autoencoder.fit(X_train, X_train, epochs=30, batch_size=128, validation_data=(X_valid, X_valid))

X_train = X_train.reshape(-1, 28, 28, 1)
X_valid = X_valid.reshape(-1, 28, 28, 1)
X_test = X_test.reshape(-1, 28, 28, 1)

autoencoder.fit(X_train, X_train, epochs=100, batch_size=128,
                validation_data=(X_valid, X_valid))

autoencoder.save('Convolution_100.h5')
# autoencoder = load_model("Convolution.h5")

X_test_decoded = autoencoder.predict(X_test)

show_images(X_test, X_test_decoded)