from __future__ import absolute_import, division, print_function, unicode_literals

# Install TensorFlow

import tensorflow as tf


mnist = tf.keras.datasets.mnist

(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0

model = tf.keras.models.Sequential([
  tf.keras.layers.Flatten(input_shape=(28, 28)),
  tf.keras.layers.Dense(128, activation='relu'),
  tf.keras.layers.Dropout(0.2),
  tf.keras.layers.Dense(10, activation='softmax')
])

model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])
model.fit(x_train, y_train, epochs=5)

model.evaluate(x_test,  y_test, verbose=2)

model = tf.keras.models.load_model('my_model.h5')
# model.save("my_model.h5")
# f = tf.saved_model.load('./')
# model = f.signatures["model"]
import cv2
import numpy as np
img = cv2.imread('test1.png')
img.astype(np.float32)
img = img[:,:,0]
img = 255 - img;
img = img.reshape((1, 28, 28))
pred = model.predict_classes(img)
pred2 = model.predict(img)
print(pred2)
print(pred)

